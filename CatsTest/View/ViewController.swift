////
//

import UIKit

class ViewController: UIViewController {
    let urlString = "https://api.thecatapi.com/v1/images/search?limit=5"
    private let reuseIdentifier = "Cell"
    var cats : [Cat] = []
    let viewModel = DownloderCats()
    var tableView: UITableView = {
        var tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    var timer = Timer()
    var customImage = CustomImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Cats"
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(ListCatsTableViewCell.self, forCellReuseIdentifier: reuseIdentifier)
        view.addSubview(tableView)
        setupTableView()
        DispatchQueue.main.async {
            self.viewModel.request(urlString: self.urlString) { result in
                self.cats = result
                self.tableView.reloadData()
            }
        }
        self.scheduledTimerWithTimeInterval(urlString)
    }
    
    @objc func updateCounting(_ url: String) {
        self.viewModel.cats = []
        imageCashe.removeAllObjects()
        DispatchQueue.main.async {
            self.viewModel.request(urlString: self.urlString) { result in
                self.cats = result
                self.tableView.reloadData()
            }
        }
        
    }
    
    func scheduledTimerWithTimeInterval(_ url: String) {
        self.timer = Timer.scheduledTimer(withTimeInterval: 20, repeats: true, block: { _ in
            self.updateCounting(url)
            
        })
    }
    func setupTableView() {
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            tableView.rightAnchor.constraint(equalTo: view.rightAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }

}

extension ViewController : UITableViewDelegate, UITableViewDataSource  {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cats.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! ListCatsTableViewCell
        if let url = cats[indexPath.row].url {
            if let url = URL(string: url) {
                cell.imageViewCats.loadImage(from: url)
            }
        }
        cell.labelName.text = cats[indexPath.row].id
        
        return cell
    }
    
}
