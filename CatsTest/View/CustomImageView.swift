//
//  CustomImageView.swift
//  CatsTest
//
//  Created by mac on 26.10.21.
//

import UIKit


let imageCashe = NSCache<AnyObject, AnyObject>()

class CustomImageView: UIImageView {
    var task : URLSessionDataTask!
    
    func loadImage(from url: URL) {
        image = nil
        
        if let task = task {
            task.cancel()
        }
        
        if let imageCashe = imageCashe.object(forKey: url.absoluteString as AnyObject) {
            
            self.image = image
            return
        }
        
         task = URLSession.shared.dataTask(with: url) { data, responce, error in
            
            guard let data = data, let newImage = UIImage(data: data) else {return}
             imageCashe.setObject(newImage, forKey: url.absoluteString as AnyObject)
            
            DispatchQueue.main.async {
                self.image = newImage
            }
        }
        
        task.resume()
    }
}
