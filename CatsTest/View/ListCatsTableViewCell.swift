//
//  ListCatsTableViewCell.swift
//  TestParsingUsers2
//
//  Created by mac on 12.10.21.
//

import UIKit

class ListCatsTableViewCell: UITableViewCell {
    
    var blackView: UIView = {
        var view = UIView()
        view.backgroundColor = .black
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var labelName: UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "muli-bold", size: 20)
        label.textColor = .white
        return label
    }()
    
    var imageViewCats = CustomImageView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
     super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(blackView)
        blackView.addSubview(labelName)
        blackView.addSubview(imageViewCats)
        setupBlackView()
        setupImageViewUser()
        setupLabelName()
     }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
 
    func setupBlackView() {
        blackView.layer.cornerRadius = 6
        NSLayoutConstraint.activate([
            blackView.leftAnchor.constraint(equalTo: leftAnchor, constant: 15),
            blackView.topAnchor.constraint(equalTo: topAnchor, constant: 15),
            blackView.rightAnchor.constraint(equalTo: rightAnchor, constant: -15),
            blackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10)])
    }
    
    func setupLabelName() {
        NSLayoutConstraint.activate([
            labelName.leftAnchor.constraint(equalTo: blackView.leftAnchor),
            labelName.topAnchor.constraint(equalTo: blackView.topAnchor, constant: 10)])
    }
    
    func setupImageViewUser() {
        imageViewCats.contentMode = .scaleToFill
        imageViewCats.translatesAutoresizingMaskIntoConstraints = false
        imageViewCats.layer.cornerRadius = 6
        NSLayoutConstraint.activate([
            imageViewCats.widthAnchor.constraint(equalToConstant: 100),
            imageViewCats.topAnchor.constraint(equalTo: blackView.topAnchor, constant: 10),
            imageViewCats.rightAnchor.constraint(equalTo: blackView.rightAnchor, constant: -5),
            imageViewCats.bottomAnchor.constraint(equalTo: blackView.bottomAnchor, constant: -10)])
    }
}
