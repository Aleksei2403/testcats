//
//  Downloader.swift
//  CatsTest
//
//  Created by mac on 25.10.21.
//

import Foundation
import UIKit

class DownloderCats {
    static let shared = DownloderCats()
    let session = URLSession.shared
    
    var cats = [Cat]()
    
    func request(urlString: String , completion: @escaping ([Cat]) -> Void) {
        
        guard let url = URL(string: urlString) else {return}
        URLSession.shared.dataTask(with: url) { data, response, error in
            DispatchQueue.main.async {
                if let error = error {
                    print(error)
                    return
                }
                guard let data = data else {return}
                
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                    
                    for dictionary in json as! [[String:Any]] {
                        let cat = Cat()
                        cat.id = dictionary["id"] as? String
                        cat.url = dictionary["url"] as? String
                        self.cats.append(cat)
                        if self.cats.count == 5 {
                            completion(self.cats)
                        }
                    }
                } catch let jsonError {
                    print(jsonError)
                }
            }
        }.resume()
    }
}
